$(document).ready(function() {
	
	$('#commentform').submit(function(){
		if ( $('#author').length ) {
			if( !$('#author').val() ) {
				alert('Podpis jest wymagany');
				return false;
			}
			
			if( !$('#email').val() ) {
				alert('E-mail jest wymagany');
				return false;
			}
			
			var emailReg = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
			if( !emailReg.test( $('#email').val() ) ) {
				$('#email').addClass('input-error');
				alert('E-mail jest nieprawidłowy');
				return false;
			} else {
				$('#email').removeClass('input-error');
			}
		}
		
		if( !$('#comment').val() ) {
			alert('Komentarz jest wymagany');
			return false;
		}
		
		return true;
	});
	
	
	$('#commentform').removeAttr('novalidate');
	$('#author, #email, #comment').attr('required', 'required');
	$('#email').attr('type', 'email');
	
	$('.comment-reply-link').click(function() {
		$('.comment-reply-link').show();
		$(this).hide();
	});
	
	$('#cancel-comment-reply-link').click(function() {
		$('.comment-reply-link').show();
	});
	
});
