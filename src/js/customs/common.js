function setBodyHeight() {

	$('#body').css('min-height',
		$(window).height() -
		$('#wpadminbar').outerHeight(true) -
		$('#header').outerHeight(true) - $('#footer').outerHeight(true) -
		$('#body').outerHeight(true) + $('#body').height() -
		$('#wrapper').outerHeight(true) + $('#wrapper').height()
	);
}

var sourcePath = $('#logo img').attr('src');
sourcePath = sourcePath.substring(0,sourcePath.lastIndexOf("/"))

/* =Załadowany dokument
-----------------------------------------------------------------------------*/
$(document).ready(function() {

	// Swiper

	var swiperTop = new Swiper('.swiper-top', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
				slidesPerView: 1,
        autoplay: 5000,
        autoplayDisableOnInteraction: false,
				loop: true,
				speed: 800,
				effect: 'slide',

    })

	var swiperPartners = new Swiper('#partners', {
	      spaceBetween: 0,
				slidesPerView: 5,
	      autoplay: 1100,
	      autoplayDisableOnInteraction: false,
				loop: true,
				freeMode: true,
				breakpoints: {
					1024: {
						slidesPerView: 3,
					},
				}
	  });

	var swiperReference = new Swiper('#seventh_section .swiper-container', {
        nextButton: '#seventh_section .swiper-button-next',
        prevButton: '#seventh_section .swiper-button-prev',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 20,
				loop: true,
				breakpoints: {
					767: {
						slidesPerView: 1,
						autoplay: 3000,
					}
				}
    });

	var swiperMap = new Swiper('#fourth_section .swiper-container', {
        nextButton: '#fourth_section .swiper-button-next',
        prevButton: '#fourth_section .swiper-button-prev',
				pagination: '#fourth_section .swiper-pagination',
				paginationClickable: true,
        slidesPerView: 1,
				autoplay: 3000,
        paginationClickable: true,
        spaceBetween: 50,
				loop: true,
    });

	// AOS

	AOS.init({
      disable: 'mobile'
    });

	// Lightcase

	$('a[data-rel^=lightcase]').lightcase({
		swipe: true,
		shrinkFactor: 1,
		maxWidth: 1200,
		maxHeight: 580,
	});

	// Google map

	if($("body").hasClass("page-id-24")){
		$(function initMap() {
			var proxima = {lat: 50.303226, lng: 18.666553};
			var centerer = {lat: 50.403226, lng: 18.667553};
	    var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 16,
	      center: centerer,
	    });
	    var marker = new google.maps.Marker({
	      position: proxima,
	      map: map,
				title: 'Proxima'
	    });
			// Editable string with html markup for tooltip content
			var contentString = '<div id="content">'+
					'<div id="siteNotice">'+
					'<h5 id="firstHeading" class="firstHeading">Proxima</h5>'+
					'</div>'+
					'<div id="bodyContent">'+
					'<p>ul. Sienkiewicza 10,<br />'+
					'44-100 Gliwice</p>'+
					'</div>'+
					'</div>';
			var infowindow = new google.maps.InfoWindow({
					content: contentString
			});
			// open tooltip on load
			infowindow.open(map, marker);
			// open tooltip  on click on the marker
			google.maps.event.addListener(marker, 'click', function() {
				 infowindow.open(map,marker);
			 });
		})
	};

	// Popover

	$("[data-toggle=popover]").popover({html:true});

	$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
	});

	// Match height

	$('.single-security').matchHeight();
	$('.single_reference').matchHeight();

	if( matchMedia('(max-width: 767px)').matches ) {

		$('.single-circle').matchHeight();

		if( document.body.className.match('home') ) {

			$('#header').addClass('navbar-fixed-top');

			$(window).scroll(function() { // check if scroll event happened
	      if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
	        $("#header").css("background-color", "#FFFFFF");
					$('#header #logo img').attr('src', sourcePath+'/proxima-logo@2.png');
					$('#header').css('box-shadow','0 5px 5px -5px rgba(0,0,0,.5)');
					$('#burger span').css('background','#3678ce');
	      } else {
	        $("#header").css("background-color", "transparent");
					$('#header #logo img').attr('src', sourcePath+'/proxima-logo-biale@2.png');
					$('#header').css('box-shadow','0 5px 5px -5px rgba(0,0,0,0)');
					$('#burger span').css('background','#FFFFFF');
	      }
	    });

		};

			if(!$("body").hasClass("home")){
				$('#header').addClass('navbar-fixed-top');
				$("#header").css("background-color", "#FFFFFF");
				$('#header #logo img').attr('src', sourcePath+'/proxima-logo@2.png');
				$('#header').css('box-shadow','0 5px 5px -5px rgba(0,0,0,.5)');
				$('#burger span').css('background','#3678ce');
			}

		var a = 0;
		$(window).scroll(function() {
		  var oTop = $('#sixth_section .circle-sixth').offset().top - window.innerHeight;
		  if (a == 0 && $(window).scrollTop() > oTop) {
		    $('#sixth_section h1 span').each(function() {
		      var $this = $(this),
		        countTo = $this.attr('data-count');
		      $({
		        countNum: $this.text()
		      }).animate({
		          countNum: countTo
		        },

		        {

		          duration: 3000,
		          easing: 'swing',
		          step: function() {
		            $this.text(Math.floor(this.countNum));
		          },
		          complete: function() {
		            $this.text(this.countNum);
		            //alert('finished');
		          }

		        });
		    });
		    a = 1;
		  }
		});

	}

	if( matchMedia('(max-width: 991px)').matches ) {

		var a = 0;
		$(window).scroll(function() {
		  var oTop = $('#sixth_section .circle-sixth').offset().top - window.innerHeight;
		  if (a == 0 && $(window).scrollTop() > oTop) {
		    $('#sixth_section h1 span').each(function() {
		      var $this = $(this),
		        countTo = $this.attr('data-count');
		      $({
		        countNum: $this.text()
		      }).animate({
		          countNum: countTo
		        },

		        {

		          duration: 3000,
		          easing: 'swing',
		          step: function() {
		            $this.text(Math.floor(this.countNum));
		          },
		          complete: function() {
		            $this.text(this.countNum);
		            //alert('finished');
		          }

		        });
		    });
		    a = 1;
		  }
		});

	}

	if( matchMedia('(min-width: 768px)').matches ) {

    $('#first_section .right_side').matchHeight({
        target: $('#first_section .left_side')
    });
		$('#fifth_section .right_side').matchHeight({
        target: $('#fifth_section .left_side')
    });

		if( document.body.className.match('home') ) {

			$('#header').addClass('navbar-fixed-top');

			$(window).scroll(function() { // check if scroll event happened
	      if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
	        $("#header").css("background-color", "#FFFFFF");
					$('#header #logo img').attr('src', sourcePath+'/proxima-logo@2.png');
					$('#header #nav .menu ul>li>a').css('color','rgb(26,26,26)');
					$('#header').css('box-shadow','0 5px 5px -5px rgba(0,0,0,.5)');
	      } else {
	        $("#header").css("background-color", "transparent");
					$('#header #logo img').attr('src', sourcePath+'/proxima-logo-biale@2.png');
					$('#header #nav .menu ul>li>a').css('color','rgb(238, 238, 238)');
					$('#header').css('box-shadow','0 5px 5px -5px rgba(0,0,0,0)');
	      }
	    });

	  };

		if(!$("body").hasClass("home")){
			$("#header").css("background-color", "#FFFFFF");
			$('#header #logo img').attr('src', sourcePath+'/proxima-logo@2.png');
			$('#header #nav .menu ul>li>a').css('color','rgb(26,26,26)');
			$('#header').css('box-shadow','0 5px 5px -5px rgba(0,0,0,.5)');
		};

	}

	if( matchMedia('(min-width: 992px)').matches ) {

		var a = 0;
		$(window).scroll(function() {
		  var oTop = $('#seventh_section').offset().top - window.innerHeight;
		  if (a == 0 && $(window).scrollTop() > oTop) {
		    $('#sixth_section h1 span').each(function() {
		      var $this = $(this),
		        countTo = $this.attr('data-count');
		      $({
		        countNum: $this.text()
		      }).animate({
		          countNum: countTo
		        },

		        {

		          duration: 3000,
		          easing: 'swing',
		          step: function() {
		            $this.text(Math.floor(this.countNum));
		          },
		          complete: function() {
		            $this.text(this.countNum);
		            //alert('finished');
		          }

		        });
		    });
		    a = 1;
		  }
		});

	}

	$('#burger').click(function() {
		if( $(this).hasClass('burger--close') ) {
			$('#mobilenav').fadeOut(300);
			$('#burger span').css('background','#3678ce');
			$(this).removeClass('burger--close');
		} else {
			$('#mobilenav').fadeIn(300);
			$('#burger span').css('background','#FFFFFF');
			$(this).addClass('burger--close');
		}
	});

});

/* =Załadowane zasoby
-----------------------------------------------------------------------------*/
$(window).load(function() {

	//setBodyHeight();
});


/* =Zmiana wielkości okna
-----------------------------------------------------------------------------*/
$(window).resize(function() {

	//setBodyHeight();
});
