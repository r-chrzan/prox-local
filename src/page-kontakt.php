<?php get_header(); ?>

<?php
	$obraz_wyrozniajacy = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' )[0];
	$obraz_wyrozniajacy_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
	$title_background = get_field('title_background');
	$title = get_the_title($post->ID);
	$parent = get_post($post->post_parent);
	$parent_title = get_the_title($parent);
?>

<div id="body">

	<header class="page-title-header page-title-header__map text-center">

		<div id="map"></div>

	</header>

	<div class="container">

		<div class="row">

			<section id="main">

				<div class="col-md-4">

					<div id="contant-form">

						<h4>FORMULARZ KONTAKTOWY</h4>

						<?php echo do_shortcode('[contact-form-7 id="181" title="Formularz kontaktowy"]') ?>

					</div>

				</div>

				<div class="col-md-8">

					<div class="row">

						<div id="helpline">

							<?php if (have_rows('infolinia')) : ?>

								<?php while(have_rows('infolinia')) : the_row(); ?>

									<div class="col-md-4">

										<img src="<?php echo get_template_directory_uri(); ?>/img/kontakt@2.png"
												 alt="Ikona kontaktu"
												 class="img-responsive"
												 width="110px"
												 height="110px"
												 >

										<p>

											<?php if (get_sub_field('nazwa_dzialu')) : ?>
												<strong> <?php the_sub_field('nazwa_dzialu'); ?> </strong> <br>
											<?php endif; ?>

											<?php if (get_sub_field('telefon')) : ?>
												tel.: <a href="tel:<?php the_sub_field('telefon'); ?>"><?php the_sub_field('telefon'); ?></a> <br>
											<?php endif; ?>

											<?php if (get_sub_field('mail')) : ?>
											 e-mail.: <a href="mailto:<?php the_sub_field('mail'); ?>"><?php the_sub_field('mail'); ?></a>
											<?php endif; ?>

										</p>

									</div>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

					</div>

					<div class="divider">

					</div>

					<div class="row">

						<div class="col-md-12">

							<span> <strong>Dane firmy:</strong> </span>
							<?php if (have_rows('dane_wydzialu')) : ?>
								<?php while(have_rows('dane_wydzialu')) : the_row(); ?>
									<p>
										<?php the_sub_field('nazwa'); ?> <br>
										<?php if (get_sub_field('dodatkowe_pole')) : ?>
											<?php the_sub_field('dodatkowe_pole'); ?> <br>
										<?php endif; ?>
										<?php the_sub_field('adres'); ?> <br>
										<?php the_sub_field('adres_cd'); ?> <br>
										KRS: <?php the_sub_field('krs'); ?> <br>
										NIP: <?php the_sub_field('nip'); ?>
									</p>

								<?php endwhile; ?>
							<?php endif; ?>

							<p>

								Projekt strony <br>
								<a href="http://weblider.eu/">
									<img src="<?php echo get_template_directory_uri();?>/img/logo-weblider@2.png"
											 alt="Logo Weblider"
											 class="img-responsive"
											 width="82"
											 height="14.5"
											 style="margin-top:.5rem"
											 >
								</a>


							</p>

						</div>

					</div>

				</div>

			</section> <!-- end of #main -->

		</div> <!-- end of .row -->

	</div> <!-- end of .container -->

</div> <!-- end of #body -->

<?php get_footer(); ?>
