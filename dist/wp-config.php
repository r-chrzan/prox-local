<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'proxima' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M993jop>-oRi ei%U[g+:N}P5pfSl<UTb_N7~y[-SQb&OlXk$uxPt`rqPq&qzn-q');
define('SECURE_AUTH_KEY',  '>{)>tZfdkSW|%_[x@8C86!%9u^iIoJB!f=]sU&+|kO[pQ^fwt2PX,GU_df1+W8ZA');
define('LOGGED_IN_KEY',    'r05ilUgr?eIheF)+L8wP(+G$ApXSI@L=GWllcLNF~$(iTR3rt@K:M8$p|;Q*|19d');
define('NONCE_KEY',        'Da(K!thO? /2/))w<628V3@1kGR`Nbk%F=]^/=]gbM$jq%-9s &Sn1H,0tj# Lq-');
define('AUTH_SALT',        'qVfjJ_ALd||Vy&L<:>+:FksrzM0&(I |L}!1HLl%+@;J;!TZCI><0k7Y|-;Ica:S');
define('SECURE_AUTH_SALT', 'O ?.Xz3DU-wXNB~a_gnc>f&B+Y]>dhZ1-1kHm[(yb.|ct[TTuX-$o954wn8ASE ?');
define('LOGGED_IN_SALT',   'd79OYR8(`UT7GL+E2u)z9ht/]z0As2CG<F/T{8=B=3Ou]GS4x3H-Xuj)TK`zv9(Z');
define('NONCE_SALT',       '^-ODdR:.C3Nb*=hn@BLXpRg-8GiHg=P9d~(mwwRn^ND6T~BOCYR%g}@tPm@- tmi');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
