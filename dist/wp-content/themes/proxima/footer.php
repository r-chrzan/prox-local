<footer id="footer">

  <?php if(is_front_page()) { ?>

    <section id="timeline_container" class="hidden-xs hidden-sm hidden-md">

      <div id="footer_wykres"></div>

      <div id="footer_wykres_inner"
           data-aos="slide-up"
           data-aos-duration="600"
           data-aos-anchor-placement="top-center"
           >
       </div>

      <ul class="timeline"
          data-aos="fade-down"
          data-aos-duration="1000"
          data-aos-delay="100"
          data-aos-anchor-placement="top-center"
          >

        <?php if (have_rows('timeline')) : ?>
          <?php the_sub_field('margin_top'); ?>
          <?php while(have_rows('timeline')) : the_row(); ?>

            <li style="left:<?php the_sub_field('pos_left'); ?>%; bottom:<?php the_sub_field('pos_bottom'); ?>%">

              <div class="pin">

                <span class="date"><?php the_sub_field('date'); ?></span> <br>
                <span class="desc">
                  <strong>
                    <?php the_sub_field('description'); ?>
                  </strong>
                </span>

              </div>

            </li>

          <?php endwhile; ?>

        <?php endif; ?>

      </ul>

    </section>

  <?php } ?>

  <?php if(!is_page( 'kontakt' )) { ?>

    <div class="container">

      <div class="row">

        <div class="col-md-12 text-center">

          <h1>
            Jesteśmy przekonani, że posiadamy kompetencje i środki potrzebne do należytej ochrony Twojego obiektu.
          </h1>
          <h5>
            Skontaktuj się z nami, aby oszacować koszt ochorny - wycenę przygotowujemy w 48 godzin, bez zobowiązań.
          </h5>

          <button>
            <a href="<?php echo get_permalink(24) ?>">Wyślij zapytanie</a>
          </button>

        </div>

      </div> <!-- end of .row -->

    </div> <!-- end of .container -->

    <span id="footer__weblider">
      Projekt strony
      <a href="http://weblider.eu/">
        <img src="<?php echo get_template_directory_uri();?>/img/logo-weblider-biale@2.png"
             alt="Logo Weblider"
             class="img-responsive"
             width="82"
             height="14.5"
             >
      </a>
    </span>

  <?php } ?>

</footer> <!-- end of footer -->


<?php wp_footer(); ?>
</body>
</html>
