<?php get_header(); ?>

<div id="body">

	<div class="container">

		<div class="row">

			<div id="main" class="col-md-12 text-centered">

				<h1>
					<?php _e('Błąd 404', 'taco'); ?>
				</h1>
				<p>
					<?php _e('Nie znaleziono strony', 'taco'); ?>
				</p>

			</div> <!-- end of #main -->

		</div><!-- end of .row -->

	</div> <!-- end of .container -->

</div> <!-- end of #body -->

<?php get_footer(); ?>
