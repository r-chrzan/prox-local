<?php get_header(); ?>

<div id="body">

	<div class="container">

		<div class="row">

			<div id="main" class="col-md-8">

				<?php while( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class() ?>>

						<header class="post_header">

							<h1 class="post_header_title">
								<a href="<?php the_permalink(); ?>"
									 title="<?php the_title_attribute(); ?>">
										<?php the_title(); ?>
								</a>
							</h1>

							<div class="post_header_date">

								<?php _e('Data', 'taco'); ?>:

								<time datetime="<?php echo get_the_date('c') ?>">
									<?php the_time(get_option('date_format')); ?>
								</time>

							</div>

							<?php if( comments_open() ) : ?>

							<div class="post_header_comments">

								<?php comments_popup_link(__('Brak komentarzy', 'taco'), __('Jeden komentarz', 'taco'), __('Komentarzy: %', 'taco')); ?>

							</div>

							<?php endif; ?>

							<div class="post_header_author">

								<?php echo get_avatar(get_the_author_meta('ID'), 36); ?></span>
								<?php _e('Autor', 'taco'); ?>: <?php the_author_posts_link(); ?>

							</div>

							<div class="post_header_categories">

								<?php _e('Kategoria', 'taco'); ?>:
								<?php the_category(', '); ?>

							</div>

							<?php the_tags('<div class="post_header_tags">'.__('Tagi', 'taco').': ', ', ', '</div>'); ?>

						</header> <!-- end of .post_header -->

						<div class="post_body">

							<?php
								if(has_post_thumbnail())
									the_post_thumbnail( array(960,240), array('alt'=>$post->post_name, 'class'=>'post__content__thumbnail') );
							?>
							<div class="post_body_content">
								<?php the_content(); ?>
							</div>

						</div> <!-- end of .post_body -->

						<footer class="post_footer">

							<ul class="pagination">
								
								<li>
									<?php next_post_link('%link', '&laquo; '.__('Nowszy wpis', 'taco'), true ); ?>
								</li>

								<li style="float:right;">
									<?php previous_post_link('%link', __('Starszy wpis', 'taco').' &raquo;', true ); ?>
								</li>

							</ul>

						</footer> <!-- end of .post_footer -->

						<?php comments_template(); ?>

					</article>

				<?php endwhile; ?>

			</div><!-- end of #main -->

			<?php get_sidebar(); ?>

		</div><!-- end of .row -->

	</div> <!-- end of .container -->

</div> <!-- end of #body -->

<?php get_footer(); ?>
