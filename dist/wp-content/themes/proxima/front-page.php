<?php get_header(); ?>

<div id="body">

	<section id="slider_top">

		<!-- Swiper -->
    <div class="swiper-top swiper-container" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/naglowek.jpg)">
        <div class="swiper-wrapper">

					<?php if (have_rows('slider_top')) : ?>

						<?php while(have_rows('slider_top')) : the_row(); ?>

							<div class="swiper-slide"

									 >

								<div class="swiper-slide-content">

									<h1>
										<?php the_sub_field('tittle'); ?>
									</h1>
									<a href="<?php the_sub_field('left_button_link'); ?>">
										<?php the_sub_field('left_button_text'); ?>
									</a>
									<a href="<?php the_sub_field('right_button_link'); ?>">
										<?php the_sub_field('right_button_text'); ?>
									</a>

								</div>

							</div>

						<?php endwhile; ?>

					<?php endif; ?>

        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"
						 data-aos="fade-left"
						 data-aos-delay="500"
						 >
			  </div>
        <div class="swiper-button-prev"
						 data-aos="fade-right"
						 data-aos-delay="500"
						 >
				</div>

				<div id="partners" class="swiper-container">
					<div class="swiper-wrapper">
						<?php if (have_rows('partners')) : ?>
							<?php while(have_rows('partners')) : the_row(); ?>
								<div class="swiper-slide text-center">
									<img src="<?php the_sub_field('parnter_logo'); ?>"
											 alt="<?php the_sub_field('parnter_title'); ?>">
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>

				<div class="slider_more visible-lg">
					<a href="#first_section"
						 rel="smooth"
						 >
						<img class="img-responsive"
								 src="<?php echo get_template_directory_uri(); ?>/img/wiecej@2.png"
								 alt="Zobacz więcej"
								 data-aos="fade-up"
								 data-aos-offset="0"
								 data-aos-delay="600"
								 data-aos-anchor="#slider-top"
								 >
					</a>
				</div>

    </div>



	</section> <!-- end of #slider_top -->

	<section id="first_section">

		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="left_side">

						<?php the_field('profile_description', 6) ?>

						<div class="row"
								 data-aos="zoom-in-up"
								 data-aos-duration="1000"
								 >

							<?php if (have_rows('circles_title')) : ?>
								<?php $licznik=1; ?>
								<?php while(have_rows('circles_title')) : the_row(); ?>

									<a href="<?php the_sub_field('what_section'); ?>"
										 rel="smooth"
										 >

										<div class="col-md-4 col-xs-6 single-circle circle-<?php echo $licznik ?>">

											<strong class="circle-title">
												<?php the_sub_field('circle_title'); ?>
											</strong>

										</div>

									</a>

									<?php $licznik++ ?>
								<?php endwhile; ?>
							<?php endif; ?>

						</div> <!-- end of .left_side .row -->

						<div class="mouse hidden-xs hidden-sm"
								 data-aos="fade-up"
								 data-aos-anchor-placement="bottom-bottom"
								 >
							<div class="scroll">

							</div>
						</div>

					</div>

				</div>

				<div class="col-md-6 hidden-xs">

					<div class="right_side map"
							 data-aos="fade-left"
							 data-aos-duration="1000"
							 data-aos-anchor-placement="center-center"
						   >

					</div>

				</div>

			</div>

		</div>

		<div class="divider_1 visible-lg"
				 data-aos="fade-in"
				 data-aos-duration="1000"
				 >

		</div>

	</section>

	<section id="second_section">

		<div class="container">

			<div class="row">

				<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-6">

					<div class="description">

						<div class="circle-second">

						</div>

						<?php the_field('second_description', 6) ?>

					</div>

				</div>

			</div>

		</div>

		<div class="divider_2 visible-lg"
				 data-aos="fade-in"
				 data-aos-duration="1000"
				 >

	</section>

	<section id="third_section">

		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="left_side">

						<div class="circle-third">

						</div>

						<?php the_field('cadre_description', 6) ?>

					</div>

				</div>

				<div class="col-md-6 hidden-xs">

					<div class="right_side">

						<ul class='circle-container'>
							<?php if (have_rows('cadre')) : ?>
								<?php $licznik = 1 ?>
								<?php while(have_rows('cadre')) : the_row(); ?>

									<?php $zdjecie = get_sub_field('photo'); ?>
									<li>
										<img src="<?php echo $zdjecie[url]; ?>"
												 alt="<?php echo $zdjecie[alt]; ?>"
												 class="pracownik-<?php echo $licznik; ?>"
												 data-toggle="popover"
												 data-placement="left"
												 data-trigger="click"
												 data-content="
												 <strong> <?php the_sub_field('name') ?> </strong> <br>
												 <p> <?php the_sub_field('position') ?> </p>
												 <a href='<?php echo esc_url( get_permalink(20) ); ?>'>
													 Dowiedz się więcej
												 </a>
												 "
												 data-container="body">
									</li>
									<?php $licznik++ ?>
								<?php endwhile; ?>

							<?php endif; ?>
						</ul>

					</div>

				</div>

			</div>

		</div>

		<div class="divider_3 visible-lg"
				 data-aos="fade-in"
				 data-aos-duration="1000"
				 >

	</section>

	<section id="fourth_section">

		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="left_side">

						<div class="circle-fourth">

						</div>

						<?php the_field('fourth_description', 6) ?>

						<?php if (have_rows('elements')) : ?>

							<?php while(have_rows('elements')) : the_row(); ?>

								<div class="single-element">

									<img src="<?php the_sub_field('element_image'); ?>" alt="<?php the_sub_field('element_title'); ?>">

									<?php the_sub_field('element_description'); ?>

								</div>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>

				</div>

				<div class="col-md-6 hidden-xs hidden-sm">

					<div class="right_side">

						<div class="swiper-container">

							<div class="swiper-wrapper">

								<?php if (have_rows('slider')) : ?>

									<?php while(have_rows('slider')) : the_row(); ?>

											<div class="swiper-slide">

												<?php the_sub_field('content'); ?>

											</div>

									<?php endwhile; ?>

								<?php endif; ?>

							</div> <!-- end of #3th .swiper-wrapper -->

							<div class="swiper-pagination"></div>

							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>

						</div> <!-- end of #3th .swiper-container -->

					</div>

				</div>

			</div>

		</div>

		<div class="patrol-1"
				 data-aos="fade-up"
				 data-aos-delay="300"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-2"
				 data-aos="fade-down"
				 data-aos-delay="500"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-3"
				 data-aos="fade-down"
				 data-aos-delay="800"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-4"
				 data-aos="fade-down"
				 data-aos-delay="1100"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-5"
				 data-aos="fade-down"
				 data-aos-delay="1400"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-6"
				 data-aos="fade-down"
				 data-aos-delay="1700"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-7"
				 data-aos="fade-down"
				 data-aos-delay="2000"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-8"
				 data-aos="fade-down"
				 data-aos-delay="2300"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-9"
				 data-aos="fade-down"
				 data-aos-delay="2400"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-10"
				 data-aos="fade-down"
				 data-aos-delay="2700"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>
		<div class="patrol-11"
				 data-aos="fade-down"
				 data-aos-delay="3000"
				 data-aos-anchor-placement="top-center"
				 >
	  </div>

		<div class="divider_4 visible-lg"
				 data-aos="fade-in"
				 data-aos-duration="1000"
				 >

	</section>

	<section id="fifth_section">

		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="left_side">

						<div class="circle-fifth">

						</div>

						<?php the_field('fifth_description', 6) ?>

						<?php if (have_rows('fifth_elements')) : ?>

							<?php while(have_rows('fifth_elements')) : the_row(); ?>

								<div class="single-element"
										 data-aos="fade-right"
										 >

									<img src="<?php the_sub_field('element_image'); ?>" alt="<?php the_sub_field('element_title'); ?>">

									<?php the_sub_field('element_description'); ?>

								</div>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>

				</div>

				<div class="col-md-6">

					<div class="right_side">

					</div>

				</div>

			</div>

		</div>

		<div class="divider_5 visible-lg"
				 data-aos="fade-in"
				 data-aos-duration="1000"
				 >

	</section>

	<section id="sixth_section">

		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="left_side">

						<div class="row">

							<div class="col-xs-6 col-md-4">

								<h1> <span data-count="<?php the_field('liczba_zadowolanych', 6) ?>"> 0 </span>%</h1>
								<p class="text-lowercase">
									<?php the_field('liczba_zadowolanych_text', 6) ?>
								</p>

							</div>

							<div class="col-xs-6 col-md-4">

								<h1> <span data-count="<?php the_field('liczba_obiektow', 6) ?>"> 0 </span> </h1>
								<p class="text-lowercase">
									<?php the_field('liczba_obiektow_text', 6) ?>
								</p>

							</div>

							<div class="col-xs-6 col-md-4">

								<h1> <span data-count="<?php the_field('liczba_zatrudnionych', 6) ?>"> 0 </span> </h1>
								<p class="text-lowercase">
									<?php the_field('liczba_zatrudnionych_text', 6) ?>
								</p>

							</div>

							<div class="col-xs-6 col-md-4">

								<h1> <span data-count="<?php the_field('wartosc_ubezpieczenia', 6) ?>"> 0 </span> mln </h1>
								<p class="text-lowercase">
									<?php the_field('wartosc_ubezpieczenia_text', 6) ?>
								</p>

							</div>

							<div class="col-xs-6 col-md-4">

								<h1> <span data-count="<?php the_field('sredni_wzrost', 6) ?>"> 0 </span>%</h1>
								<p class="text-lowercase">
									<?php the_field('sredni_wzrost_text', 6) ?>
								</p>

							</div>

							<div class="col-xs-6 col-md-4">

								<h1> <span data-count="<?php the_field('ilosc_reklamacji', 6) ?>"> 0 </span> </h1>
								<p class="text-lowercase">
									<?php the_field('ilosc_reklamacji_text', 6) ?>
								</p>

							</div>

						</div>

					</div>

				</div>

				<div class="col-md-6">

					<div class="right_side">

						<div class="circle-sixth">

						</div>

						<?php the_field('sixth_description', 6) ?>

					</div>

				</div>

			</div>

		</div>

	</section>

	<section id="seventh_section">

		<div class="container">

			<div class="row">

				<div class="swiper-container">

		        <div class="swiper-wrapper">

							<?php
								$args = array(
									'post_type' => 'referencja',
									'posts_per_page' => -1,
									'tax_query' => array(
											array(
												'taxonomy' => 'kategorie_referencji',
												'field'    => 'slug',
												'terms'    => 'strona-glowna',
											),
										),
								);
								$query = new WP_Query($args);
							?>
							<?php if($query->have_posts()) : ?>

									<?php while ($query->have_posts()) :
										$query->the_post();
										$obraz_wyrozniajacy = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' )[0];
										?>

										<div class="swiper-slide">

											<div class="col-md-6">
												<a href="<?php echo $obraz_wyrozniajacy; ?>"
													 data-rel="lightcase">
													<div class="reference-image">
													 <img src="<?php echo $obraz_wyrozniajacy; ?>"
 															 alt=""
 															 class="img-responsive">
													</div>
												</a>
											</div>

											<div class="col-md-6">
												<h5><strong><?php the_title(); ?></strong></h5>
												<?php the_excerpt(); ?>
											</div>

										</div>

									<?php endwhile; ?>

							<?php endif; ?>
							<?php wp_reset_query(); ?>

						</div> <!-- end of #7th .swiper-wrapper -->

						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>

				</div> <!-- end of #7th .swiper-container -->

				<button>
					<a href="#">Zobacz wszystkie referencje</a>
				</button>

			</div> <!-- end of #7th .row -->

		</div> <!-- end of #7th .container -->

	</section>

</div> <!-- end of #body -->

<?php get_footer(); ?>
