<?php get_header(); ?>

<?php
	$obraz_wyrozniajacy = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' )[0];
	$obraz_wyrozniajacy_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
	$title_background = get_field('title_background');
	$title = get_the_title($post->ID);
	$parent = get_post($post->post_parent);
	$parent_title = get_the_title($parent);
?>

<div id="body">

	<header class="page-title-header text-center" style="background-image:url(<?php echo $title_background; ?>)">

		<h1>
			<?php echo is_page( array(10, 12, 14)) ? $parent_title . '&nbsp' . $title : the_title(); ?>
		</h1>

	</header>

	<div class="container">

		<div class="row">

			<div id="main">

				<div class="<?php echo empty($obraz_wyrozniajacy) ? 'col-md-12' : 'col-md-6'; ?>">

					<?php
						while( have_posts() ) {
							the_post();
							the_content();
						}
					?>

				</div>

				<?php if (!empty($obraz_wyrozniajacy)) { ?>
					<div class="col-md-6">

						<img src="<?php echo $obraz_wyrozniajacy; ?>"
							 	 alt="<?php echo $obraz_wyrozniajacy_alt; ?>"
								 class="img-responsive featured-image">

					</div>
				<?php } ?>

			</div> <!-- end of #main -->

		</div> <!-- end of .row -->

	</div> <!-- end of .container -->

	<section id="referencje">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<h1>Nasze referencje</h1>

				</div>

				<?php
					$args = array(
						'post_type' => 'referencja',
						'posts_per_page' => -1,
						'tax_query' => array(
								array(
									'taxonomy' => 'kategorie_referencji',
									'field'    => 'slug',
									'terms'    => 'obiektow-przemyslowo-magazynowych',
								),
							),
					);
					$query = new WP_Query($args);
				?>
				<?php if($query->have_posts()) : ?>
						<?php $licznik = 1; ?>
						<?php while ($query->have_posts()) :
							$query->the_post();
							$obraz_wyrozniajacy = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' )[0];
							$obraz_wyrozniajacy_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
							?>

							<div class="col-xs-6 col-md-4">

								<div class="row">

									<div class="single_reference">

										<div class="col-md-6">

											<a href="<?php echo $obraz_wyrozniajacy; ?>"
												 data-rel="lightcase">
												<div class="reference-image">
												 <img src="<?php echo $obraz_wyrozniajacy; ?>"
														 alt="<?php echo $obraz_wyrozniajacy_alt; ?>"
														 class="img-responsive">
												</div>
											</a>

										</div>

										<div class="col-md-6">
											<h5><strong><?php the_title(); ?></strong></h5>
											<?php the_excerpt(); ?>
										</div>

									</div>

								</div>

							</div>

							<?php if($licznik % 3 == 0) echo '<div class="col-md-12 hidden-xs"><div class="divider"></div></div>' ?>

							<?php $licznik++ ?>
						<?php endwhile; ?>

				<?php endif; ?>
				<?php wp_reset_query(); ?>

			</div>

		</div>

	</section>

</div> <!-- end of #body -->

<?php get_footer(); ?>
