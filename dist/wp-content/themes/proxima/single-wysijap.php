<?php get_header(); ?>


<div id="body"><div class="wrapper">
	<div class="units-row end">
		
		<div id="main" class="unit-100">
			<?php
			while( have_posts() ) {
				the_post();
				the_content();
			}
			?>
		</div>
		
	</div>
</div></div>


<?php get_footer(); ?>
