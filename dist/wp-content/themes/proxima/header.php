<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title><?php wp_title('|', true, 'right'); ?></title>

  <link rel="shortcut icon"
        type="image/x-icon"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon.ico">
  <link rel="apple-touch-icon"
        sizes="57x57"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon"
        sizes="60x60"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon"
        sizes="72x72"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon"
        sizes="76x76"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon"
        sizes="114x114"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon"
        sizes="120x120"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon"
        sizes="144x144"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon"
        sizes="152x152"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon"
        sizes="180x180"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png"
        sizes="192x192"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png"
        sizes="32x32"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png"
        sizes="96x96"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png"
        sizes="16x16"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
  <link rel="manifest"
        href="<?php echo get_template_directory_uri(); ?>/img/favicon/manifest.json">
  <meta name="msapplication-TileColor"
        content="#ffffff">
  <meta name="msapplication-TileImage"
        content="<?php echo get_template_directory_uri(); ?>/img/favicon/ms-icon-144x144.png">
  <meta name="theme-color"
        content="#ffffff">

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="burger">
  <span>&nbsp;</span>
  <span>&nbsp;</span>
  <span>&nbsp;</span>
</div>

<header id="header" class="navbar">

  <div class="container">

    <div class="row">

      <div id="nav-top">

        <div id="logo" class="pull-left">
          <a href="<?php echo home_url(); ?>"
             rel="home">
              <img src="<?php echo get_template_directory_uri(); ?>/img/proxima-logo-biale@2.png"
                   alt="<?php bloginfo('name'); ?>"
                   class="img-responsive"
                   width="163"
                   height="50"
                   >
          </a>
        </div> <!-- end of #logo -->

        <div id="mobilenav">

          <nav id="nav" class="navbar navbar-pills navbar-right">
              <?php
                wp_nav_menu(array(
                  'menu'=>'primary-navigation',
                  'theme_location'=>'primary-navigation',
                  'depth'=>1
                ));
              ?>
          </nav>

        </div> <!-- end of #mobilenav -->

      </div>

    </div> <!-- end of .row -->

  </div> <!-- end of .container -->

</header> <!-- end of header -->
