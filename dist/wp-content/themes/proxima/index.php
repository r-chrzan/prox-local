<?php get_header(); ?>
<div id="body">

	<div class="container">

		<div class="row">

			<div id="main" class="col-md-8">

				<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class() ?>>

						<header class="post_header">

							<h1 class="post_header_title">
								<a href="<?php the_permalink(); ?>"
									 title="<?php the_title_attribute(); ?>">
										<?php the_title(); ?>
								</a>
							</h1>

							<div class="post_header_date">

								<?php _e('Data', 'taco'); ?>:

								<time datetime="<?php echo get_the_date('c') ?>">
									<?php the_time(get_option('date_format')); ?>
								</time>

							</div>

							<?php if( comments_open() ) : ?>

							<div class="post_header_comments">

								<?php comments_popup_link(__('Brak komentarzy', 'taco'), __('Jeden komentarz', 'taco'), __('Komentarzy: %', 'taco')); ?>

							</div>

							<?php endif; ?>

							<div class="post_header_author">

								<?php echo get_avatar(get_the_author_meta('ID'), 36); ?></span>
								<?php _e('Autor', 'taco'); ?>: <?php the_author_posts_link(); ?>

							</div>

							<div class="post_header_categories">

								<?php _e('Kategoria', 'taco'); ?>:
								<?php the_category(', '); ?>

							</div>

							<?php the_tags('<div class="post_header_tags">'.__('Tagi', 'taco').': ', ', ', '</div>'); ?>

						</header> <!-- end of .post_header -->

						<div class="post_body">

							<?php
							if(has_post_thumbnail())
								the_post_thumbnail( array(960,240), array('alt'=>$post->post_name, 'class'=>'post__content__thumbnail') );
							?>
							<div class="post_body_excerpt">
								<?php the_excerpt(); ?>
							</div>

						</div> <!-- end of .post_body -->

						<footer class="post__footer"></footer>

					</article> <!-- end of #post -->

			<?php endwhile;

				{ if(function_exists('taco_pagination')) taco_pagination(); }

			else : ?>

				<article id="post-0" class="post no-result not-found">

					<header class="post_header">

						<h1>
							<?php _e('Niczego nie znaleziono', 'taco'); ?>
						</h1>

					</header>

					<div class="post_body">

						<p>
							<?php _e('Przepraszamy, ale niczego nie znaleźliśmy pod tym adresem.', 'taco'); ?>
						</p>

					</div>

				</article>

			<?php endif; ?>

			</div><!-- end of #main -->

		<?php get_sidebar(); ?>

		</div> <!-- end of .row -->
	</div> <!-- end of .container -->
</div> <!-- end of #body -->

<?php get_footer(); ?>
