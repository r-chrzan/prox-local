<?php get_header(); ?>

<?php
	$obraz_wyrozniajacy = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' )[0];
	$obraz_wyrozniajacy_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
	$title_background = get_field('title_background');
	$title = get_the_title($post->ID);
	$parent = get_post($post->post_parent);
	$parent_title = get_the_title($parent);
?>

<div id="body">

	<header class="page-title-header text-center" style="background-image:url(<?php echo $title_background; ?>)">

		<h1>
			<?php echo is_page( array(10, 12, 14)) ? $parent_title . '&nbsp' . $title : the_title(); ?>
		</h1>

	</header>

	<div class="container">

		<div class="row">

			<section id="main">

				<div class="<?php echo empty($obraz_wyrozniajacy) ? 'col-md-12' : 'col-md-6'; ?>">

					<?php
						while( have_posts() ) {
							the_post();
							the_content();
						}
					?>

				</div>

				<?php if (!empty($obraz_wyrozniajacy)) { ?>
					<div class="col-md-6">

						<img src="<?php echo $obraz_wyrozniajacy; ?>"
							 	 alt="<?php echo $obraz_wyrozniajacy_alt; ?>"
								 class="img-responsive featured-image">

					</div>
				<?php } ?>

			</section> <!-- end of #main -->

		</div> <!-- end of .row -->

	</div> <!-- end of .container -->

</div> <!-- end of #body -->

<?php get_footer(); ?>
