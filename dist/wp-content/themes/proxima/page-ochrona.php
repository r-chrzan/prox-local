<?php get_header(); ?>

<?php
	$obraz_wyrozniajacy = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' )[0];
	$obraz_wyrozniajacy_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
	$title_background = get_field('title_background');
	$przemyslowe = get_the_title(10);
	$przemyslowe_link = get_permalink(10);
	$biurowe = get_the_title(12);
	$biurowe_link = get_permalink(12);
	$handlowe = get_the_title(14);
	$handlowe_link = get_permalink(14);
?>

<div id="body">

	<header class="page-title-header text-center"
					style="background-image:url(<?php echo $title_background; ?>)">

		<h1> <?php the_title(); ?> </h1>

	</header>

	<div class="container">

		<div class="row">

			<section id="main">

				<div class="<?php echo empty($obraz_wyrozniajacy) ? 'col-md-12' : 'col-md-6'; ?>">

					<?php
						while( have_posts() ) {
							the_post();
							the_content();
						}
					?>

				</div>

				<?php if (!empty($obraz_wyrozniajacy)) { ?>
					<div class="col-md-6">

						<img src="<?php echo $obraz_wyrozniajacy; ?>"
							 	 alt="<?php echo $obraz_wyrozniajacy_alt; ?>"
								 class="img-responsive featured-image">

					</div>
				<?php } ?>

			</section> <!-- end of #main -->

		</div> <!-- end of .row -->

	</div> <!-- end of .container -->

	<section id="our_security">

		<div class="container">

			<div class="row no-gutters">

				<div class="col-md-4">

					<div class="single-security">

						<div class="security-circle-1">

						</div>
						<?php the_title(); ?> <br>
						<?php echo $przemyslowe ?> <br>
						<a href="<?php echo $przemyslowe_link ?>">Zobacz szczegóły</a>

					</div>

				</div>

				<div class="col-md-4">
					<div class="single-security middle">
						<div class="security-circle-2">

						</div>
						<?php the_title(); ?> <br>
					  <?php echo $biurowe ?> <br>
						<a href="<?php echo $biurowe_link ?>">Zobacz szczegóły</a>
					</div>
				</div>

				<div class="col-md-4">
					<div class="single-security">
						<div class="security-circle-3">

						</div>
						<?php the_title(); ?> <br>
					  <?php echo $handlowe ?> <br>
						<a href="<?php echo $handlowe_link ?>">Zobacz szczegóły</a>
					</div>
				</div>

			</div>

		</div>

	</section>

</div> <!-- end of #body -->

<?php get_footer(); ?>
