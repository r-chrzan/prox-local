<aside id="sidebar" class="col-md-4">

	<?php if( !dynamic_sidebar('sidebar') ) : ?>

		<div class="widget">

			<h4 class="widget_header"><?php _e('Brak widgetów', 'taco'); ?></h4>

			<p><?php _e('Widgety można dodawać w panelu administracyjnym', 'taco'); ?></p>

		</div>

	<?php endif; ?>

</aside> <!-- end of #sidebar -->
