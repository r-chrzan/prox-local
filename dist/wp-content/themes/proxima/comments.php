<?php if ( post_password_required() ) return; ?>

<aside id="comments">

	<?php if ( have_comments() ) : ?>

		<h2 id="comments__title">
			<?php _e('Komentarze', 'taco'); ?>
		</h2>

		<ul id="comments__list" class="commentlist">
			<?php
				wp_list_comments( array(
					'style'		=> 'ul',
					'short_ping' 	=> false,
					'avatar_size'	=> 54,
				) );
			?>
		</ul><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<ul class="pagination">
				<li><?php previous_comments_link(  '&laquo; '.__( 'Starsze komentarze', 'taco' ) ); ?></li>
				<li style="float:right;"><?php next_comments_link( __( 'Nowsze komentarze', 'taco' ). ' &raquo;' ); ?></li>
			</ul>
		<?php endif; // Check for comment navigation. ?>

		<?php if ( !comments_open() ) : ?>
			<div class="tools-alert tools-alert-yellow">
				<?php _e( 'Dodawanie komentarzy jest zablokowane.', 'taco' ); ?>
			</div>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php
		comment_form( array(
			'comment_notes_before'	=> '',
			'comment_notes_after'	=> '',
			'fields' => array(
				'author' =>
					'<p class="comment-form-author"><label for="author">' . __( 'Imię', 'taco' ) . '</label> ' .
					//( $req ? '<span class="required">*</span>' : '' ) .
					'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
					'" size="30"' . $aria_req . ' /></p>',

				/* brak 'url' powoduje jego usuniecie */

				'email' =>
					'<p class="comment-form-email"><label for="email">' . __( 'Email', 'taco' ) . '</label> ' .
					'<span class="forms-desc">'.__('Twój email nie zostanie opublikowany.', 'taco').'</span>' .
					//( $req ? '<span class="required">*</span>' : '' ) .
					'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
					'" size="30"' . $aria_req . ' /></p>',
			),
		) );
	?>

</aside><!-- #comments -->
