<?php
/* THEME
---------------------------------------------------------------------- */

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

function script_tag_defer($tag, $handle) {
    if (is_admin()){
        return $tag;
    }
    if (strpos($tag, '/wp-includes/js/jquery/jquery')) {
        return $tag;
    }
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !==false) {
    return $tag;
    }
    else {
        return str_replace(' src',' defer src', $tag);
    }
}
add_filter('script_loader_tag', 'script_tag_defer',10,2);

add_action( 'after_setup_theme', 'taco_setup_theme' );
function taco_setup_theme() {
	// Obsługa ikon postów
		add_theme_support( 'post-thumbnails' ); // if ( has_post_thumbnail() ) { the_post_thumbnail(); }
	// Wsparcie dla HTML5
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form' ) );
	// Dodanie funkcji menu nawigacyjnych do zaczepu init
		add_action( 'init', 'taco_register_menus' );
	// Dodanie funkcji pasków bocznych do zaczeu widgets_init
		add_action( 'widgets_init', 'taco_register_widgets' );
	// Dodanie do kolejki ładowania plików JavaScript
		add_action( 'wp_enqueue_scripts', 'taco_load_scripts' );
	// Tłumaczenie motywu
		load_theme_textdomain( 'taco', get_template_directory() . '/languages' );
	// Modyfikacja tytułu
		add_filter( 'wp_title', 'taco_filter_title' );
}

function taco_filter_title( $title ) {
	global $post;

	if( is_feed() )
		return $title;

	$filtered_title  = $title . get_bloginfo( 'name' );

	if( ( $desc = get_bloginfo('description') ) && ( is_home() || is_front_page() ) )
		$filtered_title .=  ' | ' . $desc;

	if( $paged = get_query_var( 'paged' ) )
		$filtered_title .= ' | ' . __( 'Strona', 'taco' ) . ' ' . $paged;

	return $filtered_title;
}

$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );

function taco_register_menus() {
	register_nav_menu(
		'primary-navigation',
		'Menu główne'
	);
	// register_nav_menu(
	// 	'language-switcher',
	// 	'Przełącznik języka'
	// );
}

function taco_register_widgets() {
	// register_sidebar(
	// 	array(
	// 		'name'		=> 'Pasek boczny',
	// 		'id'		=> 'sidebar',
	// 		'description'   => '',
	// 		'class'		=> '',
	// 		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	// 		'after_widget'  => '</div>',
	// 		'before_title'  => '<h4 class="widget__header">',
	// 		'after_title'   => '</h4>',
	// 	)
	// );
	// register_sidebar(
	// 	array(
	// 		'name'		=> 'Stopka',
	// 		'id' 		=> 'footer',
	// 		'description'   => '',
	// 		'class'		=> '',
	// 		'before_widget' => '<li id="%1$s" class="widget %2$s">',
	// 		'after_widget'  => '</li>',
	// 		'before_title'  => '<h4 class="widget__header">',
	// 		'after_title'   => '</h4>',
	// 	)
	// );
	// USUWANIE STANDARDOWYCH WIDGETÓW
		unregister_widget('WP_Widget_Pages');		// Lista stron w serwisie.
		unregister_widget('WP_Widget_Calendar');	// Kalendarz z wpisami.
		unregister_widget('WP_Widget_Archives');	// Archiwum wpisów, podzielone na miesiące.
		unregister_widget('WP_Widget_Links');		//
		unregister_widget('WP_Widget_Meta');		// Odnośniki do: logowania, panelu administracji i WordPress.org.
		unregister_widget('WP_Widget_Search');		// Formularz wyszukiwania dla serwisu.
		unregister_widget('WP_Widget_Text');		// Dowolny tekst lub kod HTML.
		unregister_widget('WP_Widget_Categories');	// Lista lub rozwijalne menu z listą kategorii
		unregister_widget('WP_Widget_Recent_Posts');	// Najnowsze wpisy na stronie.
		unregister_widget('WP_Widget_Recent_Comments');	// Najnowsze komentarze umieszczone na stronie.
		unregister_widget('WP_Widget_RSS');		// Wpisy z dowolnego kanału RSS lub Atom.
		unregister_widget('WP_Widget_Tag_Cloud');	// Chmura najczęściej używanych tagów.
		unregister_widget('WP_Nav_Menu_Widget');	// Dodaj własne menu do panelu bocznego.

}

function taco_load_scripts() {

	if( !is_admin() ){
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', array(), '2.1.4');
    wp_enqueue_script('jquery');

    wp_enqueue_script( 'vendorjs', get_template_directory_uri() . '/js/vendors/vendors.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/customs/customs.js', array('jquery'), '1.0', true );

    wp_enqueue_script( 'google-maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyCoPPuFY3RkBZUV9ws-AdiUH6fFLmCfF1A', array('jquery'), '0.1', true );

    wp_enqueue_style( 'font', '//fonts.googleapis.com/css?family=Montserrat:400,600,600|Open+Sans:400,600,700', array(), null, 'all' );

    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), null, 'all' );
    wp_enqueue_style( 'swiper', get_template_directory_uri() . '/css/swiper.css', array(), null, 'all' );
    wp_enqueue_style( 'lightcase', get_template_directory_uri() . '/css/lightcase.css', array(), null, 'all' );
    wp_enqueue_style( 'aos', get_template_directory_uri() . '/css/aos.css', array(), null, 'all' );
	}

	// if( is_singular() && comments_open() ) {
	// 	if( get_option( 'thread_comments' ) ) {
	// 		wp_enqueue_script( 'comment-reply' );
	// 	}
	// 	wp_enqueue_script( 'commentform', $templat_dir . '/js/commentform.js', array('jquery'), '1.0', true );
	// }
}

/* LOGIN PAGE
---------------------------------------------------------------------- */

add_action( 'login_enqueue_scripts', 'login_page_style' );
function login_page_style() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo( 'template_directory' ) . '/css/wp-login.css" />';
}


function login_logo_url( $url ) {
	return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'login_logo_url' );


function login_logo_url_title( $title ) {
	return __( 'Strona główna', 'taco' );
}
add_filter( 'login_headertitle', 'login_logo_url_title' );

/* DASHBOARD
---------------------------------------------------------------------- */

function taco_wysiwyg_styles() { // zmiana stylów w WYSIWYG
    add_editor_style( get_bloginfo( 'template_directory' ) . '/css/bootstrap.min.css' );
    add_editor_style( get_bloginfo( 'template_directory' ) . '/css/custom-wysiwyg.css' );
}
add_action( 'admin_init', 'taco_wysiwyg_styles' );

/* FUNCTIONS
---------------------------------------------------------------------- */
// Możliwość zmiany ilości wyrazów w zajawce
function custom_excerpt_length( $length ) {
    return 12; // You can change the number here as per your need.
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Zmiana znaczika czytaj więcej
function wpdocs_excerpt_more( $more ) {
    return ' (...)';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function tempDir() {
    return get_template_directory_uri();
}

function get_widgets_count( $sidebar_id )
{
  $sidebars_widgets = wp_get_sidebars_widgets();
  return (int) count( (array) $sidebars_widgets[ $sidebar_id ] );
}

function taco_pagination( $html_id='' ) {
    if( is_singular() )
        return;

    global $wp_query;

    /* Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /* Add current page to the array */
    if( $paged >= 1 )
        $links[] = $paged;

    /* Add the pages around the current page to the array */
    if( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul id="' . $html_id . '" class="pagination">';

    /* Previous Post Link
    if( $paged == 1 )
        echo '<li><span class="nospan">' . __('&laquo; Poprzednia strona', 'taco') . '</span></li>';
    else
        echo '<li>' . get_previous_posts_link( __('&laquo; Poprzednia strona', 'taco') ) . '</li>';*/

    /* Link to first page, plus ellipses if necessary */
    if( !in_array( 1, $links ) ) {

        if( $paged == 1 ) {
            echo '<li><span>1</span></li>';
        } else {
            echo '<li><a href="' . get_pagenum_link(1) . '">1</a></li>';
        }

        if( !in_array( 2, $links ) )
            echo '<li><span class="nospan">…</span></li>';
    }

    /* Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        if( $paged == $link ) {
            echo '<li class="pagi-curr"><span>' . $link . '</span></li>';
        } else {
            echo '<li><a href="' . get_pagenum_link($link) . '">' . $link . '</a></li>';
        }
    }

    /* Link to last page, plus ellipses if necessary */
    if( ! in_array( $max, $links ) ) {
        if( ! in_array( $max - 1, $links ) )
            echo '<li><span class="nospan">…</span></li>';

        if( $paged == $max ) {
            echo '<li><span>' . $max . '</span></li>';
        } else {
            echo '<li><a href="' . get_pagenum_link($max) . '">' . $max . '</a></li>';
        }
    }

    /* Next Post Link
    if( $paged == $max )
        echo '<li><span class="nospan">' . __('Następna strona &raquo;', 'taco') . '</span></li>';
    else
        echo '<li>' . get_next_posts_link( __('Następna strona &raquo;', 'taco') ) . '</li>';*/

    echo '</ul>';

} // end taco_pagination()

function addImg( $nazwaImg, $tytulImg, $szerokosc, $wysokosc, $klasaImg ) {
    $path = get_template_directory_uri();

    // brak polylang, tlumaczenie OFF
    // $tytulImg = pll__($tytulImg);
    $pathImg = '<img src="'.$path.'/img/'.$nazwaImg.'" alt="'.$tytulImg.'" title="'.$tytulImg.'" ';
    if ($szerokosc && $szerokosc > 0) {
        $pathImg .= 'width="'.$szerokosc.'" ';
    }
    if ($wysokosc && $wysokosc > 0) {
        $pathImg .= 'height="'.$wysokosc.'" ';
    }
    if ($klasaImg) {
        $pathImg .= 'class="'.$klasaImg.'" ';
    }
    $pathImg .= '/>';

    echo $pathImg;
}

function wp_clear_head() {
	// Czyszczenie nagłówka z kanałów RSS
	remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
  remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 ); // shortlink
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
  remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
  remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
}
add_action('init', 'wp_clear_head');
