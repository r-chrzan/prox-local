/**
* Gulpfile
*
* @author Rafał
* @version 1.0
*/

var gulp          = require('gulp');
    sass          = require('gulp-sass'),
    browserSync   = require('browser-sync').create(),
    autoprefixer  = require('gulp-autoprefixer'),
    concat        = require('gulp-concat'),
    sourcemaps    = require('gulp-sourcemaps'),
    uglify        = require('gulp-uglify'),
    imagemin      = require('gulp-imagemin')
    shell         = require('gulp-shell'),
    runSequence   = require('run-sequence'),
    rename        = require('gulp-rename'),
    minifycss     = require('gulp-uglifycss'),
    cache         = require('gulp-cache'),
    clean         = require('gulp-clean'),
    gutil         = require('gulp-util'),
    reload        = browserSync.reload;


var project       = 'proxima',
    src           = 'src/',
    projectdist   = '/'+project,
    dist          = 'dist/wp-content/themes/'+project,
    projectUrl    = 'testowy.dev/',

    styleSRC      = src+'scss/style.scss',
    styleDEST     = dist,

    phpSRC        = src+'**/*.php',
    phpDEST       = dist,

    cssSRC        = src+'css/**/*.css',
    cssDEST       = dist+'/css',

    jsVendorSRC   = src+'js/vendors/**/*.js',
    jsVendorDEST  = dist+'/js/vendors',

    jsCustomSRC   = src+'js/customs/**/*.js',
    jsCustomDEST  = dist+'/js/customs',

    fontsSRC      = src+'fonts/*(*.eot|*.svg|*.ttf|*.woff|*.woff2)',
    fontsDEST     = dist+'/fonts'

    imgSRC        = src+'img/*(*.png|*.jpg|*.jpeg|*.gif|*.svg)',
    imgDEST       = dist+'/img',

    faviconSRC    = src+'img/favicon/*(*.png|*.ico|*.xml|*.json)'
    faviconDEST   = dist+'/img/favicon',

    functionSRC   = src+'function/**/*.php'
    functionDEST  = dist+'/function'

    styleWatch    = src+'scss/**/*.scss',
    phpWatch      = src+'**/*.php';

var autoprefixerOptions = {
  browsers: [
    'last 2 versions',
    '> 1%',
    'ie >= 9',
    'ff > 40',
    'ie_mob >= 10',
    'android >= 4'
  ]
};

gulp.task('default', ['watch']);

gulp.task('watch',
  [
  'browserSync',
  'php',
  'function',
  'styles',
  'css',
  'fonts',
  'vendorjs',
  'customjs',
  'images',
  'favicon',
  'screenshot',
  ],
  function() {
    gulp.watch( styleWatch , [ 'styles' ] );
    gulp.watch( phpSRC , [ 'php' ] ).on( 'change', reload );
    gulp.watch( fontsSRC , [ 'fonts' ] ).on( 'change', reload );
    gulp.watch( imgSRC , [ 'images' ] ).on( 'change', reload );
    gulp.watch( jsVendorSRC , [ 'vendorjs' ] ).on( 'change', reload );
    gulp.watch( jsCustomSRC , [ 'customjs' ] ).on( 'change', reload );
    gulp.watch( functionSRC , [ 'function' ] ).on( 'change', reload );
});

/**
* General Tasks
*/

// gulp browser-sync
gulp.task('browserSync', function() {
    browserSync.init({
        proxy        : projectUrl,
        open         : false,
        injectChanges: true,
        online       : true,
        // port         : 7000
    });
});

// gulp php
gulp.task( 'php', function() {
  return gulp.src( phpSRC )
    .pipe( gulp.dest( phpDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zmieniono pliki .php' ) ); } );
});

// gulp function
gulp.task( 'function', function() {
  return gulp.src( functionSRC )
    .pipe( gulp.dest( functionDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zmieniono pliki /function' ) ); } );
});

// gulp sass
gulp.task( 'styles', function() {
  return gulp.src( styleSRC )
    .pipe( sourcemaps.init())
    .pipe( sass().on( 'error', sass.logError ) )
    .pipe( autoprefixer( autoprefixerOptions) )
    .pipe( sourcemaps.write() )
    .pipe( gulp.dest( styleDEST ) )
    .pipe( browserSync.stream() )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zmieniono style.css' ) ); } );
});

// gulp css
gulp.task( 'css', function() {
  return gulp.src( cssSRC )
    .pipe( gulp.dest( cssDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zmieniono pliki .css' ) ); } );
});

// gulp vendorjs
gulp.task( 'vendorjs', function() {
  return gulp.src( jsVendorSRC )
    .pipe( concat( 'vendors.js' ) )
    .pipe( gulp.dest( jsVendorDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zmieniono vendors.js' ) ); } );
});

// gulp customjs
gulp.task( 'customjs', function() {
  return gulp.src( jsCustomSRC )
    .pipe( concat( 'customs.js' ) )
    .pipe( gulp.dest( jsCustomDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zmieniono customs.js' ) ); } );
});

// gulp fonts
gulp.task( 'fonts', function() {
  return gulp.src( fontsSRC )
    .pipe( gulp.dest( fontsDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Przeniesiono czcionki' ) ); } );
});

// gulp images
gulp.task( 'images', function() {
  return gulp.src( imgSRC )
    .pipe( cache(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true,
    })))
    .pipe( gulp.dest( imgDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zminifikowano obrazy' ) ); } );
});

// gulp theme favicon
gulp.task( 'favicon', function() {
  return gulp.src( faviconSRC )
    .pipe( gulp.dest( faviconDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Przeniesiono favicony' ) ); } );
});

// gulp theme picture
gulp.task( 'screenshot', function() {
  return gulp.src( src+'screenshot.png' )
    .pipe( gulp.dest( dist ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Przeniesiono screenshot.png' ) ); } );
});

// gulp cache:clear
gulp.task( 'cache:clear', function (callback) {
  return cache.clearAll(callback)
  .on('end', function(){ gutil.log( gutil.colors.blue( 'Wyczyszczono cache' ) ); } );
})

/**
* Build tasks
*/

// gulp clean
gulp.task('clean', function() {
  return gulp.src('dist/*', {read: false})
    .pipe(clean())
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Wyczyszczono folder dist' ) ); } );
});

// gulp sass-min
gulp.task( 'styles-min', function() {
  return gulp.src( styleSRC )
    .pipe( sass().on( 'error', sass.logError ) )
    .pipe( autoprefixer( autoprefixerOptions) )
    .pipe( minifycss( { maxLineLen: 0 } ) )
    .pipe( gulp.dest( styleDEST ) )
    .pipe( browserSync.stream() )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zminifikowano style.css' ) ); } );
});

// gulp vendorjs
gulp.task( 'vendorjs-min', function() {
  return gulp.src( jsVendorSRC )
    .pipe( concat( 'vendors.js' ) )
    .pipe(uglify())
    .pipe( gulp.dest( jsVendorDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zminifikowano vendors.js' ) ); } );
});

// gulp customjs
gulp.task( 'customjs-min', function() {
  return gulp.src( jsCustomSRC )
    .pipe( concat( 'customs.js' ) )
    .pipe(uglify())
    .pipe( gulp.dest( jsCustomDEST ) )
    .on('end', function(){ gutil.log( gutil.colors.blue( 'Zminifikowano customs.js' ) ); } );
});

/**
 * WP-CLI Tasks.
 */

 gulp.task('install-wp', shell.task([
   "wp core download --path='/var/www/app/"+project+"/dist'",
 ]));

 gulp.task('config-db', shell.task([
   "wp core config --path='/var/www/app/"+project+"/dist' --dbname="+project+" --dbuser='root' --dbpass='root' --dbhost='localhost' --dbprefix='wp_'",
 ]));

 gulp.task('create-db', shell.task([
   "wp db create --path='/var/www/app/"+project+"/dist'",
 ]));

 gulp.task('core-install', shell.task([
   "wp core install --path='/var/www/app/"+project+"/dist' --url='"+projectUrl+project+"/dist' --title='"+project+"' --admin_user='preadmin' --admin_password='terefere1' --admin_email='rafal.chrzan1@gmail.com'",
 ]));

 gulp.task('language-wp', shell.task([
   "wp core language install pl_PL --path='/var/www/app/"+project+"/dist'",
   "wp core language activate pl_PL --path='/var/www/app/"+project+"/dist'"
 ]))

 gulp.task('install-plugins', shell.task([
   "wp plugin install contact-form-7 --path='/var/www/app/"+project+"/dist'",
   "wp plugin install custom-post-type-ui --path='/var/www/app/"+project+"/dist'",
   "wp plugin install mousewheel-smooth-scroll --path='/var/www/app/"+project+"/dist'",
   "wp plugin install simple-share-buttons-adder --path='/var/www/app/"+project+"/dist'",
   "wp plugin install tinymce-advanced --path='/var/www/app/"+project+"/dist'",
   "wp plugin install wps-hide-login --path='/var/www/app/"+project+"/dist'",
   "wp plugin install wordpress-seo --path='/var/www/app/"+project+"/dist'",
   "wp plugin install disable-comments --path='/var/www/app/"+project+"/dist'",
   "wp plugin install loco-translate --path='/var/www/app/"+project+"/dist'",
   "wp plugin install polylang --path='/var/www/app/"+project+"/dist'",
 ]));

 gulp.task('delete-plugins', shell.task([
   "wp plugin uninstall akismet hello --path='/var/www/app/"+project+"/dist'",
 ]))

/**
 * Build Task.
 */

 gulp.task('wp-build', function (callback) {
   runSequence(
     'clean',
     'install-wp',
     'config-db',
     'create-db',
     'core-install',
     'language-wp',
     'install-plugins',
     'delete-plugins',
     callback
   );
 });

gulp.task('theme-build', function (callback) {
  runSequence(
    'php',
    'styles-min',
    'css',
    'fonts',
    'vendorjs-min',
    'customjs-min',
    'images',
    'favicon',
    'screenshot',
    callback
  );
});
